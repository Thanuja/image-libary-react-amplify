import * as poseDetection from '@tensorflow-models/pose-detection';
import * as tf from '@tensorflow/tfjs-core';
// Register one of the TF.js backends.
import '@tensorflow/tfjs-backend-webgl';
// import '@tensorflow/tfjs-backend-wasm';

//import * as tfjsWasm from '@tensorflow/tfjs-backend-wasm';
//tfjsWasm.setWasmPaths(
//    `https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm@${tfjsWasm.version_wasm}/dist/`);

// import {Camera} from './camera';
// import {setupDatGui} from './option_panel';
// import {STATE} from './params';
// import {setupStats} from './stats_panel';
// import {setBackendAndEnvFlags} from './util';

let detector, camera, stats;
let startInferenceTime, numInferences = 0;
let inferenceTimeSum = 0, lastPanelUpdate = 0;
let rafId;

export const createDetector = async () => {
    // return posedetection.createDetector(STATE.model, {
    //     quantBytes: 4,
    //     architecture: 'MobileNetV1',
    //     outputStride: 16,
    //     inputResolution: { width: 500, height: 500 },
    //     multiplier: 0.75
    // });  
    const detectorConfig = {modelType: poseDetection.movenet.modelType.SINGLEPOSE_LIGHTNING};
    //detector = await poseDetection.createDetector(poseDetection.SupportedModels.MoveNet, detectorConfig).catch(e => console.log('Error: ', e.message));;    

    // Create a detector.
    detector = await poseDetection.createDetector(poseDetection.SupportedModels.MoveNet, {modelType: poseDetection.movenet.modelType.SINGLEPOSE_THUNDER});


}

export const getPoses = async (image) => {

    const poses = await detector.estimatePoses(image).catch(e => console.log('Error: ', e.message));
    console.log(poses);
}

