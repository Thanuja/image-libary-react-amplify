import React from 'react'
import "./HomePage.css";
import ImageGallery from './ImageGallery';


import {Storage, API, graphqlOperation } from 'aws-amplify';
import { listPictures, searchPictures } from "../graphql/queries";
import ReactPaginate from 'react-paginate';


class HomePage extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      images: [],
      loading: true,
      currentPage: 1,
      itemsPerPage: 1,
      currentImages: [],
      offset: 0, 
      pageCount: 1,
    }
    
    this.handlePageClick = this.handlePageClick.bind(this);

  }

  async componentDidMount(){
    this.state.loading = true;
    const results = await API.graphql(graphqlOperation(listPictures));
    let imageArray = await this.buildImageArray(results.data.listPictures.items);
    //console.log(imageArray);    
    this.setState({images: imageArray});
    this.setState({currentImages: imageArray});
    this.state.loading = false;

    this.initPagination();
  }

  async buildImageArray(listPictures){
    return await this.getImagesFileList(listPictures);
  }

  async getImagesFileList(imageList){
    return Promise.all(imageList.map(async img => {
      return this.getOneFormatedImage(img);
    }));
  }

  async getOneFormatedImage(img){
    return {
      src: await Storage.get(img.file.key),
      id: img.id,
      labels: img.labels,
    };

  };

  async initPagination(){
    // const indexOfLastItem = this.state.currentPage * this.state.itemsPerPage;
    // const indexOfFirstItem = indexOfLastItem - this.state.itemsPerPage;
    let currentItems = this.state.images.slice(this.state.offset, this.state.offset + this.state.itemsPerPage);
    
    console.log(currentItems);
    this.setState({pageCount: Math.ceil(this.state.images.length / this.state.itemsPerPage)});
    this.setState({currentImages: currentItems});        

  }

  async paginate(pageNumber){
    this.state.currentPage = pageNumber;
    this.initPagination();
  }

  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.itemsPerPage;

    console.log("selectedPage: " + selectedPage )
    console.log("offset: " + offset )

    this.setState({
        currentPage: selectedPage,
        offset: offset
    }, () => {
        this.initPagination()
    });

}

  render(){
    return (
      <div className="HomePage">
        <div>Image Gallery</div>
        <ImageGallery images={this.state.currentImages} />
        <ReactPaginate
          previousLabel={"prev"}
          nextLabel={"next"}
          breakLabel={"..."}
          breakClassName={"break-me"}
          pageCount={this.state.pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={"pagination"}
          subContainerClassName={"pages pagination"}
          activeClassName={"active"}/>
      </div>
    )
  }


}

export default HomePage
