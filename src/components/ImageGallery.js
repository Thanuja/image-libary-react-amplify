import React from 'react'
import { Link, Route, useRouteMatch } from 'react-router-dom';
import Image from './Image'
import { useHistory } from "react-router-dom";

// Try paginate with this..
// https://blog.logrocket.com/4-ways-to-render-large-lists-in-react/

class ImageGallery extends React.Component {

  constructor(props) {
    super(props);

  }

  componentDidMount(){

  }

  handleChange = (e) => {
    console.log(e.target);
  }

  render() {
    
    return (
      <React.Fragment>
        <div className="container">
          {
            this.props.images.map((image) =>
              <div key={image.id} className="border border-primary rounded p-3 m-3">                
                {/* <img src={image.src} alt="alt text" width="300" />
                <div>{image.labels.join(',')}</div> */}
                <Image image={image} />
              </div>              
            )}
        </div>
      </React.Fragment >
    )
  }


}

export default ImageGallery;
