import React from 'react'
import Canvas from './Canvas';

class Image extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            image: null
        }
    }

    async componentDidMount()
    {
        console.log("loaded");
    }



    render(){
        console.log('image page');
        return(
            <div>                
                {/* <img src={this.props.image.src} alt="alt text" width="300" onLoad={() => this.handleImageLoad(this.props.image.src)} /> */}
                <p><Canvas url={this.props.image.src} id={this.props.image.id} /></p>
                <div>{this.props.image.labels.join(',')}</div>
            </div>
        )
    }

}
export default Image;
