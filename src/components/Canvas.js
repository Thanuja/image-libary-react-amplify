import React from 'react'
import {createDetector, getPoses} from './MoveNet'

//https://blog.cloudboost.io/using-html5-canvas-with-react-ff7d93f5dc76
const Canvas = ({url, id}) => {

    const onImageLoad = () => {
        const canvas = document.getElementById("canvas");
        const ctx = canvas.getContext("2d")
        const img = document.getElementById("image");

        ctx.drawImage(img, 0, 0)

        //runMoveNet(img)

    }


    const runMoveNet = (image) => {
        console.log('Image object: '+ image)
        
        // Run MoveNet
        if(image != null){
            createDetector();
            //getPoses(image);
        }
        
        // const img = document.getElementById("image");
        // console.log("image object: " + img);
        
        

        // Load the image onto Canvas allowing re-drawing..



    }


    return (
        <div key={id} id={id}>
            <canvas id={"canvas"} width={640} height={480} />
            <img id={"image"} src={url} style={{display: 'none'}} onLoad={() => onImageLoad()} />
        </div>
    )
}

export default Canvas