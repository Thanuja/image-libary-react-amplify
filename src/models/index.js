// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Picture, S3Object } = initSchema(schema);

export {
  Picture,
  S3Object
};