import json
#from flask import jsonify, Flask
import os
import boto3

TABLE_NAME=os.environ.get('API_IMAGELIBARYREACTAMPL_PICTURETABLE_NAME')
client = boto3.client('dynamodb')

## Reference: 
# https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Streams.Lambda.Tutorial.html
def handler(event, context):
    print('received event:')
    print(event)

    try:
        for record in event["Records"]:
            if record["eventName"] == 'INSERT':
                result = handle_insert(record)
                print(result)

    except Exception as e:
        print(e)
        return "Trigger failed.."


def handle_insert(record):
    print("initializing handle new record")
    # get image from the database
    
    # call Pose estimation functions.

    # get Pose. 

    # update table with the JSON string of joint positions
    pose = "{score: 0.8, keypoints: [{x: 230, y: 220, score: 0.9, name: 'nose'}, {x: 212, y: 190, score: 0.8, name: 'left_eye'}, ] }"
    result = update_item(record, pose)
    print(result)
    return "new record handled."


def update_item(record, annotations):
    record_id = record["dynamodb"]["Keys"]["id"]
    print(record_id['S'])
    print(annotations)
    print("updating record.")

    client.update_item(
        TableName=TABLE_NAME,
        Key={'id': {'S': record_id['S']}},
        UpdateExpression='Set #annotations  = :annotations',
        ExpressionAttributeNames={
            '#annotations': 'annotations'
        },
        ExpressionAttributeValues={
            ':annotations': {'S': annotations}
        }
    )
    message = "item updated"
    print(message)

    return message



def list_items():
    data=client.scan(TableName=TABLE_NAME)
    # wrap with jsonify(data)

    return data